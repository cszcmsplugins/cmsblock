<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CSZ CMS
 *
 * An open source content management system
 *
 * Copyright (c) 2016, Astian Foundation.
 *
 * Astian Develop Public License (ADPL)
 * 
 * This Source Code Form is subject to the terms of the Astian Develop Public
 * License, v. 1.0. If a copy of the APL was not distributed with this
 * file, You can obtain one at http://astian.org/about-ADPL
 * 
 * @author	CSKAZA
 * @copyright   Copyright (c) 2016, Astian Foundation.
 * @license	http://astian.org/about-ADPL	ADPL License
 * @link	https://www.cszcms.com
 * @since	Version 1.0.0
 */

class Cmsblock_model extends CI_Model {
    
    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    public function insert() {
        // Create the new lang
        ($this->input->post('active')) ? $active = $this->input->post('active', TRUE) : $active = 0;
        $content2 = $this->input->post('content', FALSE);
        $content1 = str_replace('&lt;', '<', $content2);
        $content = str_replace('&gt;', '>', $content1);
        $custom_css = str_replace(array('<style type="text/css">',"<style type='text/css'>",'<style>','</style>'), '', $this->input->post('custom_css', FALSE));
        $custom_js = str_replace(array('<script type="text/javascript">',"<script type='text/javascript'>",'<script>','</script>'), '', $this->input->post('custom_js', FALSE));
        $cmsblock_name_input = str_replace('&amp;', '&', $this->Csz_admin_model->chkPageName($this->input->post('cmsblock_name', TRUE)));

        $data = array(
            'cmsblock_name' => $cmsblock_name_input,
            'content' => $content,
        );
        if($this->Csz_auth_model->is_group_allowed('cssjs additional', 'backend') !== FALSE){
            $data['custom_css'] = $custom_css;
            $data['custom_js'] = $custom_js;
        }
        $this->db->set('lang_iso', $this->input->post('lang_iso', TRUE));
       // $this->db->set('active', $active);
        $this->db->set('timestamp_create', 'NOW()', FALSE);
        $this->db->set('timestamp_update', 'NOW()', FALSE);
        $this->db->insert('cms_block', $data);
        //$error = $this->db->error();
        //echo "<pre>";print_r($error);die;
    }
    
    public function update($id) {
        // Create the new lang
        ($this->input->post('active')) ? $active = $this->input->post('active', TRUE) : $active = 0;
        $content2 = $this->input->post('content', FALSE);
        $content1 = str_replace('&lt;', '<', $content2);
        $content = str_replace('&gt;', '>', $content1);
        $custom_css = str_replace(array('<style type="text/css">',"<style type='text/css'>",'<style>','</style>'), '', $this->input->post('custom_css', FALSE));
        $custom_js = str_replace(array('<script type="text/javascript">',"<script type='text/javascript'>",'<script>','</script>'), '', $this->input->post('custom_js', FALSE));
        $cmsblock_name_input = str_replace('&amp;', '&', $this->Csz_admin_model->chkPageName($this->input->post('cmsblock_name', TRUE)));

        $this->db->set('cmsblock_name', $cmsblock_name_input, TRUE);
        $this->db->set('content', $content, TRUE);
        $this->db->set('lang_iso', $this->input->post('lang_iso', TRUE));
        
        if($this->Csz_auth_model->is_group_allowed('cssjs additional', 'backend') !== FALSE){
            $this->db->set('custom_css', $custom_css, TRUE);
            $this->db->set('custom_js', $custom_js, TRUE);
        }
        if($id != 1){
        //    $this->db->set('active', $active, FALSE);
        }

        $this->db->set('timestamp_update', 'NOW()', FALSE);
        $this->db->where("cmsblock_db_id", $id);
        $this->db->update('cms_block');
        $error = $this->db->error();
    }
    
    public function delete($id) {
        if($id){
            $this->Csz_admin_model->removeData('cms_block', 'cmsblock_db_id', $id);
        }else{
            return FALSE;
        }
    }
    
}
