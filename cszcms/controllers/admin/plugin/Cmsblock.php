<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * CSZ CMS
 *
 * An open source content management system
 *
 * Copyright (c) 2016, Astian Foundation.
 *
 * Astian Develop Public License (ADPL)
 * 
 * This Source Code Form is subject to the terms of the Astian Develop Public
 * License, v. 1.0. If a copy of the APL was not distributed with this
 * file, You can obtain one at http://astian.org/about-ADPL
 * 
 * @author  CSKAZA
 * @copyright   Copyright (c) 2016, Astian Foundation.
 * @license http://astian.org/about-ADPL    ADPL License
 * @link    https://www.cszcms.com
 * @since   Version 1.0.0
 */
class Cmsblock extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->lang->load('admin', $this->Csz_admin_model->getLang());
        $this->lang->load('plugin/cmsblock', $this->Csz_admin_model->getLang());
        $this->template->set_template('admin');
        $this->load->model('plugin/Cmsblock_model');
        $this->_init();
        admin_helper::plugin_not_active('cmsblock');
    }

    public function _init() {
        $this->template->set('core_css', $this->Csz_admin_model->coreCss());
        $this->template->set('core_js', $this->Csz_admin_model->coreJs());
        $this->template->set('title', 'Backend System | ' . $this->Csz_admin_model->load_config()->site_name);
        $this->template->set('meta_tags', $this->Csz_admin_model->coreMetatags('Backend System for CSZ Content Management System'));
        $this->template->set('cur_page', $this->Csz_admin_model->getCurPages());
    }

    public function index() {
        admin_helper::is_logged_in($this->session->userdata('admin_email'));
        admin_helper::is_allowchk('cmsblock');
        $this->load->library('pagination');
        $this->load->helper('form');
        $this->db->cache_on();
        $this->csz_referrer->setIndex('cmsblock'); /* Set index page when redirect after save */
        $search_arr = "1";
        if ($this->input->get('search')) {
            $search_arr.= " AND cmsblock_name LIKE '%" . $this->input->get('search', TRUE) . "%'";          
        }
        if (!$this->input->get('lang')) {
            $search_arr.= " AND lang_iso = '" . $this->Csz_model->getDefualtLang() . "'";
        }else if($this->input->get('lang')){
            $search_arr.= " AND lang_iso = '" . $this->input->get('lang', TRUE) . "'";
        }
        
        
        $result_per_page = 20;
        $total_row = $this->Csz_admin_model->countTable('cms_block', $search_arr);
        $num_link = 10;
        $base_url = $this->Csz_model->base_link(). '/admin/cmsblock/';
        // Pageination config
        $this->Csz_admin_model->pageSetting($base_url,$total_row,$result_per_page,$num_link);    
        ($this->uri->segment(3))? $pagination = ($this->uri->segment(3)) : $pagination = 0; 

        //Get users from database
        $this->template->setSub('cmsblock', $this->Csz_admin_model->getIndexData('cms_block', $result_per_page, $pagination, 'cmsblock_db_id', 'asc', $search_arr));

        $this->template->setSub('total_row', $total_row);
        $this->template->setSub('lang', $this->Csz_model->loadAllLang());

        //Load the view
        $this->template->loadSub('admin/plugin/cmsblock/cmsblock_index');
    }

    public function add() {
        admin_helper::is_logged_in($this->session->userdata('admin_email'));
        admin_helper::is_allowchk('cmsblock');
        $this->template->set('extra_js', '<script type="text/javascript">'.$this->Csz_admin_model->getSaveDraftJS().'</script>');
        //Load the form helper
        $this->load->helper('form');
        $this->template->setSub('lang', $this->Csz_model->loadAllLang());
        //Load the view
        $this->template->loadSub('admin/plugin/cmsblock/cmsblock_add');
    }

    public function insert() {
        admin_helper::is_logged_in($this->session->userdata('admin_email'));
        admin_helper::is_allowchk('cmsblock');
        admin_helper::is_allowchk('save');
        //Load the form validation library
        $this->load->library('form_validation');
        //Set validation rules
        $this->form_validation->set_rules('cmsblock_name', 'Block Name', 'required');
        if ($this->form_validation->run() == FALSE) {
            //Validation failed
            $this->add();
        } else {
            //Validation passed
            //Add the user
            $this->Cmsblock_model->insert();
            $this->output->delete_cache('/plugin/cmsblock/rss');
            $this->Csz_model->clear_file_cache('cmsblock_getWidget_*', TRUE);
            $this->db->cache_delete_all();
            redirect($this->csz_referrer->getIndex('cmsblock'), 'refresh');
        }
    }

    public function edit() {
        admin_helper::is_logged_in($this->session->userdata('admin_email'));
        admin_helper::is_allowchk('cmsblock');
        //Load the form helper
        $this->load->helper('form');
        $this->csz_referrer->setIndex('cmsblock_edit'); /* Set index page when redirect after save */
        if ($this->uri->segment(5)) {
            $this->template->setSub('cmsblock', $this->Csz_model->getValue('*', 'cms_block', 'cmsblock_db_id', $this->uri->segment(5), 1));
            $this->template->setSub('lang', $this->Csz_model->loadAllLang());
            
            $search_arr = "cmsblock_db_id = '".$this->uri->segment(5)."'";
            // Pages variable           
            // $total_row = $this->Csz_model->countData('gallery_picture', $search_arr);           
            // $this->template->setSub('showfile', $this->Csz_admin_model->getIndexData('gallery_picture', 0, 0, 'arrange', 'ASC', $search_arr));
            // $this->template->setSub('total_row', $total_row);
            //Load the view
            $this->template->loadSub('admin/plugin/cmsblock/cmsblock_edit');
        } else {
            redirect($this->csz_referrer->getIndex('cmsblock'), 'refresh');
        }
    }

    public function editSave() {
        admin_helper::is_logged_in($this->session->userdata('admin_email'));
        admin_helper::is_allowchk('cmsblock');
        admin_helper::is_allowchk('save');
        if ($this->uri->segment(5)) {
            //Load the form validation library
            $this->load->library('form_validation');
            //Set validation rules
            $this->form_validation->set_rules('cmsblock_name', 'Block Name', 'required');
           // $this->form_validation->set_rules('short_desc', 'Short Description', 'required');
            if ($this->form_validation->run() == FALSE) {
                //Validation failed
                $this->edit();
            } else {
                //Validation passed                
                $this->Cmsblock_model->update($this->uri->segment(5));
                $this->output->delete_cache('/plugin/cmsblock/rss');
                $this->Csz_model->clear_file_cache('cmsblock_getWidget_*', TRUE);
                $this->db->cache_delete_all();
                redirect($this->csz_referrer->getIndex('cmsblock'), 'refresh');
            }
        } else {
            redirect($this->csz_referrer->getIndex('cmsblock'), 'refresh');
        }
    }
    
    
    public function delete() {
        admin_helper::is_logged_in($this->session->userdata('admin_email'));
        admin_helper::is_allowchk('cmsblock');
        admin_helper::is_allowchk('delete');
        if ($this->uri->segment(5)) {
            $path = FCPATH . "/photo/plugin/cmsblock/";
            //Delete the data
            $filedel = $this->Csz_model->getValue('*', 'cmsblock_picture', 'cmsblock_db_id', $this->uri->segment(5));
            if (!empty($filedel)) {
                foreach ($filedel as $value) {
                    if ($value) {
                        if ($value->file_upload) {
                            @unlink($path . $value->file_upload);
                        }
                        $this->Csz_admin_model->removeData('cmsblock_picture', 'cmsblock_picture_id', $value->cmsblock_picture_id);
                    }
                }
            }
            $this->Cmsblock_model->delete($this->uri->segment(5));
            $this->output->delete_cache('/plugin/cmsblock/rss');
            $this->Csz_model->clear_file_cache('cmsblock_getWidget_*', TRUE);
            $this->db->cache_delete_all();
            $this->session->set_flashdata('error_message', '<div class="alert alert-success" role="alert">' . $this->lang->line('success_message_alert') . '</div>');
        }
        redirect($this->csz_referrer->getIndex('cmsblock'), 'refresh');
    }

}
