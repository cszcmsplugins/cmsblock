<?php
$lang['cmsblock_header']		= "CMS Block";
$lang['cmsblock_addnew']		= "Add Block";
$lang['cmsblock_name']			= "Name";
$lang['cmsblock_edit']			= "Edit Block";
$lang['cmsblock_indexremark']	= "<i>Please use this tag for insert the cms block into the content. Please see cmsblock ID#<br><b>Tag:</b></i> [?]{=cmsblock:<b>cmsblock_id</b>}[?]";