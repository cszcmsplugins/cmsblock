<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* For your plugin config.
 * Importnt! Please Don't change the config item index name (for systems config)
 * 
 * For load plugin config:
 * $this->Csz_model->getPluginConfig('plugin_config_filename', 'item index name');
 * Ex. $this->Csz_model->getPluginConfig('article', 'plugin_name');
 * 
 */

/* Start System Config (Important) */
/* General Config */
$plugin_config['plugin_name']  = 'CmsBlock';
$plugin_config['plugin_urlrewrite']  = 'cmsblock'; /* Please don't have any blank space */
$plugin_config['plugin_author']  = 'Xiontechnologies'; /* For your name */
$plugin_config['plugin_version']   = '1.0.0';
$plugin_config['plugin_description']   = 'Static cms block plugin'; /* For your plugin description */

/* for menu inside member zone. If not have please blank. 
 * Example: $plugin_config['plugin_member_menu'] = 'link_name';
 * The link automatic to {base_url}/plugin/{your_plugin_urlrewrite}
 * plugin_menu_permission_name is permission name from user_perms table on DB
 */
$plugin_config['plugin_member_menu'] = '';
$plugin_config['plugin_menu_permission_name'] = '';

/* Database Config */
$plugin_config['plugin_db_table']   = array(
    'cms_block',
); /* Please input all your pludin db table name */

/* Sitemap Generater Config (for content view page only) 
 * If don't want to use sitemap for your plugin. Please blank.
 */
$plugin_config['plugin_sitemap_viewtable']   = 'cms_block';
/* for sitemap sql extra condition for this view table. If not have please blank. */
$plugin_config['plugin_sqlextra_condition']   = "active = '1'";

/* Sitemap Generater Config (for content category page only) 
 * If don't want to use sitemap for your plugin. Please blank.
 */
$plugin_config['plugin_sitemap_cattable']   = '';
/* for sitemap sql extra condition for this category table. If not have please blank. */
$plugin_config['plugin_sqlextra_catcondition']   = "";

/* All your plugin file path 
 * For directory please put / into the end of path.
 * Filename or Directory name is case sensitive.
 */
$plugin_config['plugin_file_path']   = array(
    FCPATH . '/cszcms/config/plugin/cmsblock.php',
    FCPATH . '/cszcms/controllers/admin/plugin/Cmsblock.php',
    FCPATH . '/cszcms/models/plugin/Cmsblock_model.php',
    // FCPATH . '/cszcms/modules/plugin/controllers/Cmsblock.php',
    // FCPATH . '/cszcms/modules/plugin/views/templates/cszdefault/gallery/',
    FCPATH . '/cszcms/views/admin/plugin/cmsblock/',
    FCPATH . '/cszcms/language/dutch/plugin/cmsblock_lang.php',
    FCPATH . '/cszcms/language/english/plugin/cmsblock_lang.php',
    FCPATH . '/cszcms/language/italian/plugin/cmsblock_lang.php',
    FCPATH . '/cszcms/language/spanish/plugin/cmsblock_lang.php',
    FCPATH . '/cszcms/language/thai/plugin/cmsblock_lang.php',
);

/* Plugin widget Config (for content view page only) 
 * If don't have the widget for your plugin. Please blank.
 */
$plugin_config['plugin_widget_viewtable']   = 'cms_block';
/* for sql extra condition for this view table. If not have please blank. */
$plugin_config['plugin_widget_condition']   = "active = '1'";
/* for select the field from the database with array('field_name'). If not have please blank. */
$plugin_config['plugin_widget_sel_field']   = array(
    'cmsblock_db_id',
    'cmsblock_name',
    'lang_iso',
    'content',
    'custom_css',
    'custom_js',
    'active',
    'timestamp_create',
    'timestamp_update'
);


$plugin_config['plugin_custom_widget_access_code']   = 'cmsblock';
$plugin_config['plugin_custom_widget_viewtable']   = 'cms_block';
$plugin_config['plugin_custom_widget_find_by_field']   = "cmsblock_db_id";
$plugin_config['plugin_custom_widget_condition']   = "active = '1'";
$plugin_config['plugin_custom_widget_view_field']   = 'content';



/* End System Config (Important) */

/* Custom config (For your plugin config)
 * Please add your config after this section
 */
$plugin_config['backend_startup'] = '';
$plugin_config['frontend_startup'] = '';