DELETE FROM `plugin_manager` WHERE `plugin_config_filename` = 'cmsblock';
INSERT INTO `plugin_manager` (`plugin_manager_id`, `plugin_config_filename`, `plugin_active`, `timestamp_create`, `timestamp_update`) VALUES
('', 'cmsblock', 0, NOW(), NOW());
DELETE FROM `user_perms` WHERE `name` = 'CMS Block';
INSERT INTO `user_perms` (`user_perms_id`, `name`, `definition`, `permstype`) VALUES
('', 'CMS Block', 'For shop plugin access permission on backend', 'backend');
DROP TABLE IF EXISTS `cms_block`;
CREATE TABLE IF NOT EXISTS `cms_block` (
  `cmsblock_db_id` int(11)  NOT NULL,
  `cmsblock_name` varchar(255),
  `lang_iso` varchar(10),
  `content` text,
  `custom_css` text DEFAULT NULL,
  `custom_js` text DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 - In Active, 1 - Active',
  `timestamp_create` datetime DEFAULT NULL,
  `timestamp_update` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

ALTER TABLE `cms_block`
  ADD PRIMARY KEY (`cmsblock_db_id`);

ALTER TABLE `cms_block`
  MODIFY `cmsblock_db_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;