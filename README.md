# cmsblock
========================

Static CMS Block use anywhere ([CSZCMS][cszcms-url])

[cszcms-url]: https://www.cszcms.com/

## Usage

Create static html block and insert into cms page using 

    [?]{=cmsblock:cmsblock_id}[?]